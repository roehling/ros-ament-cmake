Source: ros-ament-cmake
Section: devel
Priority: optional
Maintainer: Timo Röhling <timo.roehling@fkie.fraunhofer.de>
Build-Depends: debhelper-compat (= 12), cmake, dh-python, python3-all,
               python3-ament-package, python3-catkin-pkg
Standards-Version: 4.5.0
Homepage: https://github.com/ament/ament_cmake
Rules-Requires-Root: no

Package: ament-cmake
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-export-definitions, ament-cmake-export-dependencies, ament-cmake-export-include-directories, ament-cmake-export-interfaces, ament-cmake-export-libraries, ament-cmake-export-link-flags, ament-cmake-libraries, ament-cmake-python, ament-cmake-target-dependencies, ament-cmake-test, ament-cmake-version, cmake
Description: Buildsystem for CMake based Robot OS packages
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the entry point package for the ament buildsystem
 in CMake.

Package: ament-cmake-auto
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake
Description: Buildsystem for CMake based Robot OS packages (auto)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the auto-magic functions for ease to use of the
 ament buildsystem in CMake.

Package: ament-cmake-core
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, cmake, python3-ament-package, python3-catkin-pkg
Description: Buildsystem for CMake based Robot OS packages (core)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the core of the ament buildsystem in CMake.

Package: ament-cmake-export-definitions
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (export_definitions)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export definitions to downstream
 packages in the ament buildsystem.

Package: ament-cmake-export-dependencies
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-libraries
Description: Buildsystem for CMake based Robot OS packages (export_dependencies)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export dependencies to downstream
 packages in the ament buildsystem in CMake.

Package: ament-cmake-export-include-directories
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (export_include_directories)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export include directories to
 downstream packages in the ament buildsystem in CMake.

Package: ament-cmake-export-interfaces
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-export-libraries
Description: Buildsystem for CMake based Robot OS packages (export_interfaces)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export interfaces to downstream
 packages in the ament buildsystem in CMake.

Package: ament-cmake-export-libraries
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (export_libraries)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export libraries to downstream
 packages in the ament buildsystem in CMake.

Package: ament-cmake-export-link-flags
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (export_link_flags)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to export link flags to downstream
 packages in the ament buildsystem.

Package: ament-cmake-gmock
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-gtest, ament-cmake-test, google-mock
Description: Buildsystem for CMake based Robot OS packages (gmock)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to add Google mock-based tests in the
 ament buildsystem in CMake.

Package: ament-cmake-gtest
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-test, googletest
Description: Buildsystem for CMake based Robot OS packages (gtest)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to add gtest-based tests in the ament
 buildsystem in CMake.

Package: ament-cmake-include-directories
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (include_directories)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the functionality to order include directories
 according to a chain of prefixes in the ament buildsystem in CMake.

Package: ament-cmake-libraries
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (libraries)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the functionality to deduplicate libraries in the
 ament buildsystem in CMake.

Package: ament-cmake-nose
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-test, python3-nose
Description: Buildsystem for CMake based Robot OS packages (nose)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to add nose-based tests in the ament
 buildsystem in CMake.

Package: ament-cmake-pytest
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-test, python3-pytest
Description: Buildsystem for CMake based Robot OS packages (pytest)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to run Python tests using pytest in
 the ament buildsystem in CMake.

Package: ament-cmake-python
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (python)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to use Python in the ament buildsystem
 in CMake.

Package: ament-cmake-target-dependencies
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core, ament-cmake-include-directories, ament-cmake-libraries
Description: Buildsystem for CMake based Robot OS packages (target_dependencies)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to add definitions, include
 directories and libraries of a package to a target in the ament
 buildsystem in CMake.

Package: ament-cmake-test
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ${python3:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (test)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to add tests in the ament buildsystem
 in CMake.

Package: ament-cmake-version
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, ament-cmake-core
Description: Buildsystem for CMake based Robot OS packages (version)
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
 .
 This package provides the ability to override the exported package
 version in the ament buildsystem.
