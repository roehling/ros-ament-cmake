#!/usr/bin/python3

import os
import xml.etree.ElementTree as ET
import textwrap
from itertools import chain

SHORT_DESCRIPTION = "Buildsystem for CMake based Robot OS packages"
LONG_DESCRIPTION = """\
 This package is part of Robot OS version 2 (ROS2).
 ament_cmake is a set of scripts to enhance CMake and adds convenience
 functionality for Robot OS package authors. It is particularly useful
 for packages written in C++ and Python.
"""

MAP_DEPS = {
        "python3-catkin-pkg-modules": "python3-catkin-pkg",
        "ament_package": "python3-ament-package",
        "gtest_vendor": None,
        "gmock_vendor": None,
        "gtest": "googletest",
}


def toposort(graph):
    order = {}

    def visit(node, depth):
        if order.get(node, -1) < depth:
            order[node] = depth
            for dep in graph[node]:
                visit(dep, depth + 1)

    for node in graph.keys():
        visit(node, 0)

    output = [(-depth, node) for node, depth in order.items()]
    output.sort()
    return [node for _, node in output]


debian_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
package_dir = os.path.dirname(debian_dir)

with open(os.path.join(debian_dir, "control"), "r") as f:
    control_header = f.read().split("\n\n")[0]

with open(os.path.join(debian_dir, "control.tmp"), "w") as f:
        f.write(control_header)
        f.write("\n")
        packages = [name for name in os.listdir(package_dir) if name.startswith("ament_cmake")]
        packages.sort()
        build_deps = {}
        for name in packages:
            xml = ET.parse(os.path.join(package_dir, name, "package.xml"))
            deps = set()
            deps.add("${misc:Depends}")
            for d in chain(xml.findall("./buildtool_export_depend"), xml.findall("./build_export_depend"), xml.findall("./depend")):
                d_name = MAP_DEPS.get(d.text, d.text.replace("_", "-"))
                if d_name is not None:
                    deps.add(d_name)
            b_deps = set()
            for d in chain(xml.findall("./buildtool_depend"), xml.findall("./buildtool_export_depend"), xml.findall("./build_depend"), xml.findall("./build_export_depend"), xml.findall("./depend")):
                if d.text.startswith("ament_cmake"):
                    b_deps.add(d.text)
            for d in xml.findall("./buildtool_depend"):
                if d.text == "ament_cmake_python":
                    deps.add("${python3:Depends}")
                    break
            build_deps[name] = b_deps
            package_name = name.replace("_", "-")
            module_name = name[12:] if name != "ament_cmake" else None
            module_tag = " (%s)" % module_name if module_name else ""
            module_desc = xml.find("./description").text.strip()
            module_desc = module_desc[:1].lower() + module_desc[1:]
            if "." in module_desc:
                module_desc = module_desc.split(".")[0]
            module_desc = "This package provides " + module_desc
            if not module_desc.endswith("."):
                module_desc += "."
            module_desc = " " + "\n ".join(textwrap.wrap(module_desc, 72))
            f.write(
                "\n"
                "Package: %(package)s\n"
                "Architecture: all\n"
                "Multi-Arch: foreign\n"
                "Depends: %(deps)s\n"
                "Description: %(short_desc)s%(module_tag)s\n"
                "%(long_desc)s"
                " .\n"
                "%(module_desc)s\n"
                % {
                    "package": package_name,
                    "module_tag": module_tag,
                    "short_desc": SHORT_DESCRIPTION,
                    "long_desc": LONG_DESCRIPTION,
                    "module_desc": module_desc,
                    "deps": ", ".join(sorted(deps)),
                }
            )
            with open(os.path.join(debian_dir, package_name + ".install.tmp"), "w") as g:
                g.write(
                    "usr/share/%(name)s\n"
                    "usr/share/ament_index/resource_index/*/%(name)s\n"
                    % {
                        "name": name
                    }
                )
order = toposort(build_deps)

os.rename(os.path.join(debian_dir, "control.tmp"), os.path.join(debian_dir, "control"))
with open(os.path.join(debian_dir, "build-order.make"), "w") as f:
    f.write("BUILD_ORDER = " + " ".join(order))
for name in os.listdir(debian_dir):
    if name.endswith(".install"):
        os.unlink(os.path.join(debian_dir, name))
for name in os.listdir(debian_dir):
    if name.endswith(".install.tmp"):
        os.rename(os.path.join(debian_dir, name), os.path.join(debian_dir, name[:-4]))
